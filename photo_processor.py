import threading
import time
import cv2
import numpy as np
import os
import sqlite3 as lite


db_file_name = "db.sqlite"


class PhotoProcessor(threading.Thread):
    _start_event = threading.Event()
    img_path = ''
    b_flag = False

    def __init__(self):
        threading.Thread.__init__(self)
        self.check_db()

    def run(self):
        self._start_event.clear()
        while True:
            if self._start_event.isSet():
                self.b_flag = True
                self.process_manager()
                self.b_flag = False
                self._start_event.clear()
            else:
                time.sleep(0.1)

    def process_manager(self):
        s_time = time.time()
        print "Starting process with ", self.img_path
        img = cv2.imread(self.img_path)
        height = np.size(img, 0)
        width = np.size(img, 1)
        f_name = os.path.basename(self.img_path).split('.')[0]
        f_ext = os.path.basename(self.img_path).split('.')[1]
        f_path = os.path.dirname(self.img_path)

        for i in [2, 3, 4]:
            resized_img = cv2.resize(img, (width/i, height), interpolation=cv2.INTER_CUBIC)
            full_path = os.path.join(f_path, f_name + "_thumb" + str(i) + "." + f_ext)
            cv2.imwrite(full_path, resized_img)

        # Upload to db
        try:
            conn = lite.connect(db_file_name)
            curs = conn.cursor()

            sql = "INSERT INTO tb_img (image_name, width, height) VALUES ('" + self.img_path + "', " + \
                  str(width) + ", " + str(height) + ");"
            print sql
            curs.execute(sql)
            conn.commit()
            print "Finished, Elapsed time: ", time.time() - s_time
            conn.close()
            return True
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return None

    def check_db(self):
        """
        check whether cmd_table exists and if not, create it
        :return:
        """
        conn = lite.connect(db_file_name)
        curs = conn.cursor()
        try:
            sql = 'CREATE TABLE IF NOT EXISTS tb_img (id INTEGER PRIMARY KEY AUTOINCREMENT, image_name TEXT, ' \
                  'width NUMERIC, height NUMERIC); '
            print sql
            curs.execute(sql)
            conn.commit()
            conn.close()
            return True
        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return None

    def start_process(self):
        self._start_event.set()

    def set_img_file(self, file_path):
        self.img_path = file_path

    def get_status(self):
        return self.b_flag
