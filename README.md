# Install Packages.

NOTE: This is for the Ubuntu 14.04.

## Install OpenCV

Please refer follow to install python2.7 - OpenCV3 on Ubuntu.

http://www.pyimagesearch.com/2015/06/22/install-opencv-3-0-and-python-2-7-on-ubuntu/

## Install dependencies.

    sudo pip install numpy
    sudo apt-get install sqlite3
    sudo pip install Flask
    
# Running APP.

    sudo python app.py
   
Open the browser and visit http://localhost:8080

